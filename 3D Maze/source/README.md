# Alex Bauer's Simple 3D Maze
#*Copyright April 2018*

##ABOUT 
Hello, user. This is a very simple program which allows you to navigate a small 3D maze. I created the maze using the free web-based tool TinkerCAD found
at https://www.tinkercad.com/ and exported as a .obj fle for 3D printing. This meant that the Y- and Z-axes were reversed. The Player Character 
model is the duck.obj provided by Dr. David Millman (bunny and dino did not load properly). The code for loading the .obj files was sourced from 
https://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_Load_OBJ and modified to support the specific files for this project. The bulk of
the code was based on what was provided for and added into my Lab 6 assignment. I relied heavily on the libraries provided for vectors, matrices, and the 
camera. I'm doing this not to be lazy, but because each time I use these libraries I learn something new from them. I am also guilty of utilizing GLM for 
the .obj file loading method. I am justifying that because it only requires the vec3 and vec4 objects and is only present in the code for these extra credit 
opportunities. All of the requirements for the assignment are done without GLM.

##CONTROLS
Spacebar: Toggle camera (Camera View by default, alternate to Bird's Eye View).
Arrow Keys: User navigation. UP and DOWN for forward and reverse, LEFT and RIGHT to rotate.
I, J, K, L: Camera adjustment in Camera View (Pseudo Over the Shoulder View). I, K up/down respectively and J, L left/right respectively.