#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/matrix.h>
#include <csci441/matrix3.h>
#include <csci441/vector.h>
#include <csci441/uniform.h>

#include "shape.h"
#include "model.h"
#include "camera.h"
#include "renderer.h"

#include "glm/vec4.hpp"
#include "glm/vec3.hpp"
#include "glm/glm.hpp"
#include "vector"

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;
bool topDown = false;
bool pressed = false;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_RELEASE;
}

Matrix processModel(const Matrix& model, GLFWwindow *window, Camera &camera) {
    Matrix trans;

    const float ROT = 0.3;
    const float SCALE = .05;
    const float TRANS = .01;

	if (isPressed(window, GLFW_KEY_UP)) { trans.translate(0, 0, TRANS); }
	if (isPressed(window, GLFW_KEY_DOWN)) { trans.translate(0, 0, -TRANS); }
	if (isPressed(window, GLFW_KEY_LEFT)) { trans.rotate_y(-ROT); }
	if (isPressed(window, GLFW_KEY_RIGHT)) { trans.rotate_y(ROT); }

	if (isPressed(window, GLFW_KEY_I)) {
		if (!topDown) {
			camera.origin.values[1] += 0.3;
		}
	}
	if (isPressed(window, GLFW_KEY_K)) {
		if (!topDown) {
			camera.origin.values[1] -= 0.3;
		}
	}
	if (isPressed(window, GLFW_KEY_J)) {
		if (!topDown) {
			camera.origin.values[0] -= 0.5;
		}
	}
	if (isPressed(window, GLFW_KEY_L)) {
		if (!topDown) {
			camera.origin.values[0] += 0.5;
		}
	}

    return trans * model;
}

void processInput(Matrix& model, GLFWwindow *window, Camera &camera) {
    if (isPressed(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
        glfwSetWindowShouldClose(window, true);
    }
	
	if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
		pressed = true;
	}
	else if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_RELEASE) {
		if (pressed) {
			pressed = false;
			if (topDown) {
				topDown = false;
				
			}
			else {
				topDown = true;
				camera.eye = Vector(0, 10, 0);
				camera.origin = Vector(0, 0, 0);
				camera.up = Vector(0, 0, -1);
			}
		}
	}
    model = processModel(model, window, camera);
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

//////////////////////////////////////////
//Code sourced from https://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_Load_OBJ
//////////////////////////////////////////
void load_obj(const char* filename, std::vector<glm::vec4> &vertices, std::vector<glm::vec3> &normals, std::vector<GLushort> &elements)
{
	std::ifstream in(filename, std::ios::in);
	if (!in)
	{
		std::cerr << "Cannot open " << filename << std::endl; exit(1);
	}

	std::string line;
	while (getline(in, line))
	{
		if (line.substr(0, 2) == "v ")
		{
			std::istringstream s(line.substr(2));
			glm::vec4 v; s >> v.x; s >> v.y; s >> v.z; v.w = 1.0f;
			vertices.push_back(v);
		}
		else if (line.substr(0, 2) == "f ")
		{
			std::istringstream s(line.substr(2));
			GLushort a, b, c;
			s >> a; s >> b; s >> c;
			a--; b--; c--;
			elements.push_back(a); elements.push_back(b); elements.push_back(c);
		}
		else if (line[0] == '#')
		{
			/* ignoring this line */
		}
		else
		{
			/* ignoring this line */
		}
	}

	normals.resize(vertices.size(), glm::vec3(0.0, 0.0, 0.0));
	for (int i = 0; i < elements.size(); i += 3)
	{
		GLushort ia = elements[i];
		GLushort ib = elements[i + 1];
		GLushort ic = elements[i + 2];
		glm::vec3 normal = glm::normalize(glm::cross(
			glm::vec3(vertices[ib]) - glm::vec3(vertices[ia]),
			glm::vec3(vertices[ic]) - glm::vec3(vertices[ia])));
		normals[ia] = normals[ib] = normals[ic] = normal;
	}
}
//////////////////////////////////////////

int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "CSCI441-Program 2 - Maze", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

	// coords for objs
	std::vector<float> coords;
	std::vector<float> coords2;

	///////////////////////////
	//code from https://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_Load_OBJ
	///////////////////////////
	// load maze obj
	std::vector<glm::vec4> maze_vertices;
	std::vector<glm::vec3> maze_normals;
	std::vector<GLushort> maze_elements;

	load_obj("../models/tinker.obj", maze_vertices, maze_normals, maze_elements);

	// load character obj(duck)
	std::vector<glm::vec4> char_vertices;
	std::vector<glm::vec3> char_normals;
	std::vector<GLushort> char_elements;

	load_obj("../models/duck.obj", char_vertices, char_normals, char_elements);
	///////////////////////////

	// maze calcs
	for (int i = 0; i < maze_elements.size(); i++) {
		glm::vec3 temp = { 0.0f, 0.0f, 0.0f };
		maze_normals.push_back(temp);
	}

	// load coordinates, color, and normals for smooth edges on maze because I worked hard on it and might as well use it here
	for (int i = 0; i < maze_elements.size(); i++) {
		int mazevert = maze_elements[i];

		float xc = maze_vertices[mazevert].x;
		float yc = maze_vertices[mazevert].y;
		float zc = maze_vertices[mazevert].z;


		if ((i % 3) == 2) {
			// get previous 2 vertices 
			int ia = maze_elements[i - 2];
			int ib = maze_elements[i - 1];
			int ic = maze_elements[i];

			// get normal
			float vec1x = maze_vertices[ic].x - maze_vertices[ia].x;
			float vec1y = maze_vertices[ic].y - maze_vertices[ia].y;
			float vec1z = maze_vertices[ic].z - maze_vertices[ia].z;
			float vec2x = maze_vertices[ib].x - maze_vertices[ia].x;
			float vec2y = maze_vertices[ib].y - maze_vertices[ia].y;
			float vec2z = maze_vertices[ib].z - maze_vertices[ia].z;

			float cx = vec1y * vec2z - vec1z * vec2y;
			float cy = vec1z * vec2x - vec1x * vec2z;
			float cz = vec1x * vec2y - vec1z * vec2x;

			maze_normals[i].x += cx;
			maze_normals[i].y += cy;
			maze_normals[i].z += cz;
			
			maze_normals[ia].x += cx;
			maze_normals[ia].y += cy;
			maze_normals[ia].z += cz;
			
			maze_normals[ib].x += cx;
			maze_normals[ib].y += cy;
			maze_normals[ib].z += cz;
		}
	}

	for (int i = 0; i < maze_vertices.size(); i++) {
		int count=0;
		for (int j = 0; j < maze_elements.size(); j++) {
			int look = maze_elements[j];
			if (j % 3 == 2) {
				if (i == look | i - 1 == look | i - 2 == look) {
					count += 1;
				}
			}
		}
		maze_normals[i].x = maze_normals[i].x / count;
		maze_normals[i].y = maze_normals[i].y / count;
		maze_normals[i].z = maze_normals[i].z / count;
	}

	for (int i = 0; i < maze_elements.size(); i++) { //based on add_vertex() from shape.h
													 //for vertex from list of elements, get vertex vector of coordinates
		int mazevert = maze_elements[i];

		//put coordinates in coords
		float xc = maze_vertices[mazevert].x /10;
		coords.push_back(xc);				 
		float yc = maze_vertices[mazevert].z /10;
		coords.push_back(yc);				 
		float zc = maze_vertices[mazevert].y /10;
		coords.push_back(zc);

		//make maze walls yellow
		coords.push_back(1.0);
		coords.push_back(1.0);
		coords.push_back(0.0);

		//push normals
		coords.push_back(maze_normals[mazevert].x);
		coords.push_back(maze_normals[mazevert].y);
		coords.push_back(maze_normals[mazevert].z);

	}

	// character calcs
	for (int i = 0; i < char_elements.size(); i++) {
		glm::vec3 temp = { 0.0f, 0.0f, 0.0f };
		char_normals.push_back(temp);
	}

	// load coordinates, color, and normals for smooth edges on character(duck) because I worked hard on it and might as well use it here
	for (int i = 0; i < char_elements.size(); i++) {
		int charvert = char_elements[i];

		float xc = char_vertices[charvert].x;
		float yc = char_vertices[charvert].y;
		float zc = char_vertices[charvert].z;


		if ((i % 3) == 2) {
			// get previous 2 vertices 
			int ia = char_elements[i - 2];
			int ib = char_elements[i - 1];
			int ic = char_elements[i];

			// get normal
			float vec1x = char_vertices[ic].x - char_vertices[ia].x;
			float vec1y = char_vertices[ic].y - char_vertices[ia].y;
			float vec1z = char_vertices[ic].z - char_vertices[ia].z;
			float vec2x = char_vertices[ib].x - char_vertices[ia].x;
			float vec2y = char_vertices[ib].y - char_vertices[ia].y;
			float vec2z = char_vertices[ib].z - char_vertices[ia].z;

			float cx = vec1y * vec2z - vec1z * vec2y;
			float cy = vec1z * vec2x - vec1x * vec2z;
			float cz = vec1x * vec2y - vec1z * vec2x;

			char_normals[i].x += cx;
			char_normals[i].y += cy;
			char_normals[i].z += cz;
			
			char_normals[ia].x += cx;
			char_normals[ia].y += cy;
			char_normals[ia].z += cz;
			
			char_normals[ib].x += cx;
			char_normals[ib].y += cy;
			char_normals[ib].z += cz;
		}
	}

	for (int i = 0; i < char_vertices.size(); i++) {
		int count = 0;
		for (int j = 0; j < char_elements.size(); j++) {
			int look = char_elements[j];
			if (j % 3 == 2) {
				if (i == look | i - 1 == look | i - 2 == look) {
					count += 1;
				}
			}
		}
		char_normals[i].x = char_normals[i].x / count;
		char_normals[i].y = char_normals[i].y / count;
		char_normals[i].z = char_normals[i].z / count;
	}

	for (int i = 0; i < char_elements.size(); i++) { //based on add_vertex() from shape.h
													 //for vertex from list of elements, get vertex vector of coordinates
		int charvert = char_elements[i];

		//put coordinates in coords2
		float xc = char_vertices[charvert].x / 3500;
		coords2.push_back(xc);
		float yc = char_vertices[charvert].y / 3500 + 0.75;
		coords2.push_back(yc);
		float zc = char_vertices[charvert].z / 3500;
		coords2.push_back(zc);

		//make character red
		coords2.push_back(1.0);
		coords2.push_back(0.0);
		coords2.push_back(0.0);

		//push normals
		coords2.push_back(char_normals[charvert].x);
		coords2.push_back(char_normals[charvert].y);
		coords2.push_back(char_normals[charvert].z);

	}
	//////////////////////////////////////////////////////

	// maze object
	Model obj(coords, Shader("../vert.glsl", "../frag.glsl"));

	// sphere "player" object
    Model obj2(
            /*Sphere(40, .75, 0.0f, 0.0f, 1.0f).coords,*/
			coords2,
            Shader("../vert.glsl", "../frag.glsl"));

    // make a floor
    Model floor(
            DiscoCube().coords,
            Shader("../vert.glsl", "../frag.glsl"));
    Matrix floor_trans, floor_scale;
    floor_trans.translate(0, 0, 0);
    floor_scale.scale(100, 1, 100);
    floor.model = floor_trans*floor_scale;

    // setup camera
    Matrix projection;
    projection.perspective(45, 1, .01, 10);

    Camera camera;
	// From behind (default)
    camera.projection = projection;
    camera.eye = Vector(0, 1, 0.5);
    camera.origin = Vector(0, 1, 0);
    camera.up = Vector(0, 1, 0);

	// Top down
	/*camera.projection = projection;
	camera.eye = Vector(0, 10, 0);
	camera.origin = Vector(0, 0, 0);
	camera.up = Vector(0, 0, 1);*/

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    // create a renderer
    Renderer renderer;

    // set the light position
    Vector lightPos(3.75f, 3.75f, 4.0f);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(obj.model, window, camera);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // render the maze, player, and the floor
        renderer.render(camera, obj, lightPos);
		renderer.render(camera, obj2, lightPos);
        renderer.render(camera, floor, lightPos);

		// Camera adjustment
		if (!topDown) {
			camera.projection = projection;
			camera.eye = Vector(0, 1, 0.5);
			camera.origin = Vector(0, 1, 0);
			camera.up = Vector(0, 1, 0);
		}
		
		/*else{
			camera.projection = projection;
			camera.eye = Vector(0, 10, 0);
			camera.origin = Vector(0, 0, 0);
			camera.up = Vector(0, 0, -1);
		}*/

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
