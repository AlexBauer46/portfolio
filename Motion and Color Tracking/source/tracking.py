import numpy as np
import cv2 as cv

cap = cv.VideoCapture(0)

cv.namedWindow("frame")
hMin = 0
hMax = 255
sMin = 0
sMax = 255
vMin = 0
vMax = 255



# Create Trackbars
def change_slider(value):
    cat = 1

cv.createTrackbar( "H low", "frame", hMin, 255, change_slider)
cv.createTrackbar( "H high", "frame", hMax, 255, change_slider)
cv.createTrackbar( "S low", "frame", sMin, 255, change_slider)
cv.createTrackbar( "S high", "frame", sMax, 255, change_slider)
cv.createTrackbar( "V low", "frame", vMin, 255, change_slider)
cv.createTrackbar( "V high", "frame", vMax, 255, change_slider)

# Create mouse event
def onMouse(event,x,y,flags,param):
    if event == cv.EVENT_LBUTTONDOWN:        
        print(x, y, hsv[x , y])





while(True):
    # Capture frame-by-frame
    status, frame = cap.read()

    # Our operations on the frame come here
    hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)

    # Display the resulting frame
    cv.imshow('frame', frame)
    cv.imshow('hsv', hsv)

    hMin = cv.getTrackbarPos( "H low", "frame")
    hMax = cv.getTrackbarPos( "H high", "frame")
    sMin = cv.getTrackbarPos( "S low", "frame")
    sMax = cv.getTrackbarPos( "S high", "frame")
    vMin = cv.getTrackbarPos( "V low", "frame")
    vMax = cv.getTrackbarPos( "V high", "frame")

    minS = np.array([hMin, sMin, vMin])
    maxS = np.array([hMax, sMax, vMax])

    bw = cv.inRange(hsv, minS, maxS)

    cv.imshow('bw', bw)

    
    cv.setMouseCallback('hsv', onMouse)


 
    if cv.waitKey(1) & 0xFF == ord('q'):
        break


# When everything done, release the capture
cap.release()
cv.destroyAllWindows()
