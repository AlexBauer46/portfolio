# Program 3
# RayTracing with triangles

This program generates a world consisting of two triangles and a light source. Two images are generated using ray tracing: one which uses an orthographic projection
and another using a perspective projection. Both triangles are shaded with phong shading utilizing initialized with the triangles coordinates and colors. The ray tracing
is performed to determine which of the two triangles is intersected first, and then drawn to the image. When the further triangle is intersected, it performs another
ray tracing in order to determine if it is shadowed by the other tringle. This is done by using the point of intersection as the new origin and the direction from that
point to the light source. If it does intersect that triangle, the distance is used to factor the shadow intensity on the triangle in shadow. This creates a low fidelity
pseudo soft shadow where the closer the two triangles are, the more intense the shadow and a weakening gradient shadow is produced as distance increases. The two images
produced are 1280x960 pixels in order to show a demonstrable level of difference in the gradient. The factor of the influence of the distance on shadowing has been tuned
specifically for the orthographic and perspective views I have generated in order to have the desired result.