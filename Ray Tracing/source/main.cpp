#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtx/intersect.hpp>

#include "bitmap_image.hpp"

struct Viewport {
    glm::vec2 min;
    glm::vec2 max;

    Viewport(const glm::vec2& min, const glm::vec2& max)
        : min(min), max(max) {}
};



struct Triangle {
	glm::vec3 vertA;
	glm::vec3 vertB;
	glm::vec3 vertC;

	glm::vec3 normA;
	glm::vec3 normB;
	glm::vec3 normC;

	glm::vec3 color;

	Triangle(const glm::vec3& vertA = glm::vec3(0, 0, 0), const glm::vec3& vertB = glm::vec3(1, 0, 0), const glm::vec3& vertC = glm::vec3(0, 1, 0),
		const glm::vec3& normA = glm::vec3(0, 0, 0), const glm::vec3& normB = glm::vec3(0, 0, 0), const glm::vec3& normC = glm::vec3(0, 0, 0),
		const glm::vec3& color = glm::vec3(0, 0, 0))
		: vertA(vertA), vertB(vertB), vertC(vertC), normA(normA), normB(normB), normC(normC), color(color) {
	}
};

struct Ray {
	glm::vec3 origin;
	glm::vec3 direction;

	Ray(const glm::vec3& origin, const glm::vec3& direction)
		: origin(origin), direction(direction) {}
};




void render(bitmap_image& image, const std::vector<Triangle>& world2) {
    // TODO: implement ray tracer
	for (int x = 0; x < 1280; x++) {
		for (int y = 0; y < 960; y++) {
			int l = -5;
			int r = 5;
			int t = 5;
			int b = -5;

			
			float u = l + (r - l)*(x + 0.5) / 1280;
			float v = b + (t - b)*(y + 0.5) / 960;

			// ray for orthogonal
			Ray rayij = Ray(glm::vec3(u, v, 0), glm::vec3(0, 0, 1));
			float factor = 0.07;

			// ray for perspective
			/*glm::vec3 direct = glm::normalize(glm::vec3(0, 0, 5) + glm::vec3(u, 0, 0) + glm::vec3(0, v, 0));
			Ray rayij = Ray(glm::vec3(0, 0, -5), direct);
			float factor = .05;*/

			//lighting stuff
			float ambientStrength = 0.2;
			float specularStrength = 0.5;
			glm::vec3 lightColor = glm::vec3(1.0, 1.0, 1.0);
			glm::vec3 source = glm::vec3(-1, -1, -2);
			
			//ray direction and origin
			glm::vec3 d = rayij.direction;
			glm::vec3 e = rayij.origin;
			
			//intersect with triangle 1
			glm::vec3 result;
			glm::intersectRayTriangle(e, d, world2[0].vertA, world2[0].vertB, world2[0].vertC, result);

			
			//position of intersect with triangle 1
			glm::vec3 posInt = glm::vec3((world2[0].vertA.x + (result.x*(world2[0].vertB.x - world2[0].vertA.x)) + (result.y*(world2[0].vertC.x - world2[0].vertA.x))),
				(world2[0].vertA.y + (result.x*(world2[0].vertB.y - world2[0].vertA.y)) + (result.y*(world2[0].vertC.y - world2[0].vertA.y))),
				(world2[0].vertA.z + (result.x*(world2[0].vertB.z - world2[0].vertA.z)) + (result.y*(world2[0].vertC.z - world2[0].vertA.z))));
			//normal of triangle 1 at intersection calculations
			glm::vec3 lerp1 = glm::mix(world2[0].normA, world2[0].normB, (result.x));
			glm::vec3 lerp2 = glm::mix(world2[0].normA, world2[0].normC, (result.y));
			glm::vec3 lerp3 = glm::mix(world2[0].normB, world2[0].normC, (1 - (result.x + result.y)));

			glm::vec3 lerpf = glm::vec3((lerp1.x + lerp2.x + lerp3.x), (lerp1.y + lerp2.y + lerp3.y), (lerp1.z + lerp2.z + lerp3.z));
			lerpf = glm::normalize(lerpf);

			//intersection of triangle 2
			glm::vec3 result3;
			glm::intersectRayTriangle(e, d, world2[1].vertA, world2[1].vertB, world2[1].vertC, result3);
			//position of intersection with triangle 2
			glm::vec3 posInt2 = glm::vec3((world2[1].vertA.x + (result3.x*(world2[1].vertB.x - world2[1].vertA.x)) + (result3.y*(world2[1].vertC.x - world2[1].vertA.x))),
				(world2[1].vertA.y + (result3.x*(world2[1].vertB.y - world2[1].vertA.y)) + (result3.y*(world2[1].vertC.y - world2[1].vertA.y))),
				(world2[1].vertA.z + (result3.x*(world2[1].vertB.z - world2[1].vertA.z)) + (result3.y*(world2[1].vertC.z - world2[1].vertA.z))));
			// calculation of normal at intersection with triangle 2
			glm::vec3 lerp1B = glm::mix(world2[1].normA, world2[1].normB, (result3.x));
			glm::vec3 lerp2B = glm::mix(world2[1].normA, world2[1].normC, (result3.y));
			glm::vec3 lerp3B = glm::mix(world2[1].normB, world2[1].normC, (1 - (result3.x + result3.y)));

			glm::vec3 lerpfB = glm::vec3((lerp1B.x + lerp2B.x + lerp3B.x), (lerp1B.y + lerp2B.y + lerp3B.y), (lerp1B.z + lerp2B.z + lerp3B.z));
			lerpfB = glm::normalize(lerpfB);
			
			// if intersecting triangle 1
			if (result.z > 0) {
				if (result.y <= 1 && result.y >= 0) {
					if ((1 - result.y) >= result.x) {
						if (result.x > 0) {
							if (result.x < result.z) {
								//do ray tracing from intersection to lightsource
								glm::vec3 result2;
								glm::vec3 toSource = glm::vec3(source.x - posInt.x, source.y - posInt.y, source.z - posInt.z);
								glm::intersectRayTriangle(posInt, toSource,
									world2[1].vertA, world2[1].vertB, world2[1].vertC, result2);
								//if triangle 2 is in between, create shadow
								if (result2.z > 0) {
									if (result2.y <= 1 && result2.y >= 0) {
										if ((1 - result2.y) >= result2.x) {
											if (result2.x > 0) {
												glm::vec3 ambient = ambientStrength * lightColor;

												glm::vec3 normal = glm::normalize(lerpf);
												glm::vec3 lightDir = glm::normalize(source - posInt);

												float diff = std::max(dot(normal, lightDir), 0.0f);
												glm::vec3 diffuse = diff * lightColor;

												glm::vec3 viewDir = glm::normalize(e - posInt);
												glm::vec3 reflectDir = reflect(lightDir, normal);
												float spec = pow(std::max(dot(viewDir, reflectDir), 0.0f), 32);
												glm::vec3 specular = diffuse * specularStrength * spec * lightColor;

												glm::vec3 resultC = (ambient + diffuse + specular)*world2[0].color;

												rgb_t color = make_colour(((factor*result.z) * (255*resultC.r)), ((factor*result.z) * (255*resultC.g)), 
													((factor*result.z) * (255*resultC.b)));
												image.set_pixel(x, 960 - y, color);
											}
										}
									}
								}
								// triangle 2 is not in the path
								else {
									glm::vec3 ambient = ambientStrength * lightColor;

									glm::vec3 normal = glm::normalize(lerpf);
									glm::vec3 lightDir = glm::normalize(source - posInt);

									float diff = std::max(dot(normal, lightDir), 0.0f);
									glm::vec3 diffuse = diff * lightColor;

									glm::vec3 viewDir = glm::normalize(e - posInt);
									glm::vec3 reflectDir = reflect(lightDir, normal);
									float spec = pow(std::max(dot(viewDir, reflectDir), 0.0f), 32);
									glm::vec3 specular = diffuse * specularStrength * spec * lightColor;

									glm::vec3 resultC = (ambient + diffuse + specular)*world2[0].color;

									//rgb_t color = make_colour((255 * world2[0].color.r), (255 * world2[0].color.g), (255 * world2[0].color.b));
									rgb_t color = make_colour((255 * resultC.r), (255 * resultC.g), (255 * resultC.b));
									image.set_pixel(x, 960 - y, color);
								}
							}
						}
					}
				}
			}

			//if triangle 2 is intersected
			if (result3.z > 0) {
				if (result3.y <= 1 && result3.y >= 0) {
					if ((1 - result3.y) >= result3.x) {
						if (result3.x > 0) {
							// if triangle 2 is in front of triangle 1
							if (result3.z < result.z) {
								glm::vec3 ambient = ambientStrength * lightColor;

								glm::vec3 normal = glm::normalize(lerpfB);
								glm::vec3 lightDir = glm::normalize(source - posInt2);

								float diff = std::max(dot(normal, lightDir), 0.0f);
								glm::vec3 diffuse = diff * lightColor;

								glm::vec3 viewDir = glm::normalize(e - posInt2);
								glm::vec3 reflectDir = reflect(lightDir, normal);
								float spec = pow(std::max(dot(viewDir, reflectDir), 0.0f), 32);
								glm::vec3 specular = diffuse * specularStrength * spec * lightColor;

								glm::vec3 resultC = (ambient + diffuse + specular)*world2[1].color;

								rgb_t color = make_colour((255 * resultC.r), (255 * resultC.g), (255 * resultC.b));
								//rgb_t color = make_colour((255 * world2[1].color.r), (255 * world2[1].color.g), (255 * world2[1].color.b));
								image.set_pixel(x, 960 - y, color);
							}
						}
					}
				}
			}
		}
	}
}

void renderP(bitmap_image& image, const std::vector<Triangle>& world2) {
	// TODO: implement ray tracer
	for (int x = 0; x < 1280; x++) {
		for (int y = 0; y < 960; y++) {
			int l = -5;
			int r = 5;
			int t = 5;
			int b = -5;


			float u = l + (r - l)*(x + 0.5) / 1280;
			float v = b + (t - b)*(y + 0.5) / 960;

			// ray for orthogonal
			//Ray rayij = Ray(glm::vec3(u, v, 0), glm::vec3(0, 0, 1));
			//float factor = 0.07;

			// ray for perspective
			glm::vec3 direct = glm::normalize(glm::vec3(0, 0, 5) + glm::vec3(u, 0, 0) + glm::vec3(0, v, 0));
			Ray rayij = Ray(glm::vec3(0, 0, -5), direct);
			float factor = .05;

			//lighting stuff
			float ambientStrength = 0.2;
			float specularStrength = 0.5;
			glm::vec3 lightColor = glm::vec3(1.0, 1.0, 1.0);
			glm::vec3 source = glm::vec3(-1, -1, -2);

			//ray direction and origin
			glm::vec3 d = rayij.direction;
			glm::vec3 e = rayij.origin;

			//intersect with triangle 1
			glm::vec3 result;
			glm::intersectRayTriangle(e, d, world2[0].vertA, world2[0].vertB, world2[0].vertC, result);


			//position of intersect with triangle 1
			glm::vec3 posInt = glm::vec3((world2[0].vertA.x + (result.x*(world2[0].vertB.x - world2[0].vertA.x)) + (result.y*(world2[0].vertC.x - world2[0].vertA.x))),
				(world2[0].vertA.y + (result.x*(world2[0].vertB.y - world2[0].vertA.y)) + (result.y*(world2[0].vertC.y - world2[0].vertA.y))),
				(world2[0].vertA.z + (result.x*(world2[0].vertB.z - world2[0].vertA.z)) + (result.y*(world2[0].vertC.z - world2[0].vertA.z))));
			//normal of triangle 1 at intersection calculations
			glm::vec3 lerp1 = glm::mix(world2[0].normA, world2[0].normB, (result.x));
			glm::vec3 lerp2 = glm::mix(world2[0].normA, world2[0].normC, (result.y));
			glm::vec3 lerp3 = glm::mix(world2[0].normB, world2[0].normC, (1 - (result.x + result.y)));

			glm::vec3 lerpf = glm::vec3((lerp1.x + lerp2.x + lerp3.x), (lerp1.y + lerp2.y + lerp3.y), (lerp1.z + lerp2.z + lerp3.z));
			lerpf = glm::normalize(lerpf);

			//intersection of triangle 2
			glm::vec3 result3;
			glm::intersectRayTriangle(e, d, world2[1].vertA, world2[1].vertB, world2[1].vertC, result3);
			//position of intersection with triangle 2
			glm::vec3 posInt2 = glm::vec3((world2[1].vertA.x + (result3.x*(world2[1].vertB.x - world2[1].vertA.x)) + (result3.y*(world2[1].vertC.x - world2[1].vertA.x))),
				(world2[1].vertA.y + (result3.x*(world2[1].vertB.y - world2[1].vertA.y)) + (result3.y*(world2[1].vertC.y - world2[1].vertA.y))),
				(world2[1].vertA.z + (result3.x*(world2[1].vertB.z - world2[1].vertA.z)) + (result3.y*(world2[1].vertC.z - world2[1].vertA.z))));
			// calculation of normal at intersection with triangle 2
			glm::vec3 lerp1B = glm::mix(world2[1].normA, world2[1].normB, (result3.x));
			glm::vec3 lerp2B = glm::mix(world2[1].normA, world2[1].normC, (result3.y));
			glm::vec3 lerp3B = glm::mix(world2[1].normB, world2[1].normC, (1 - (result3.x + result3.y)));

			glm::vec3 lerpfB = glm::vec3((lerp1B.x + lerp2B.x + lerp3B.x), (lerp1B.y + lerp2B.y + lerp3B.y), (lerp1B.z + lerp2B.z + lerp3B.z));
			lerpfB = glm::normalize(lerpfB);

			// if intersecting triangle 1
			if (result.z > 0) {
				if (result.y <= 1 && result.y >= 0) {
					if ((1 - result.y) >= result.x) {
						if (result.x > 0) {
							if (result.x < result.z) {
								//do ray tracing from intersection to lightsource
								glm::vec3 result2;
								glm::vec3 toSource = glm::vec3(source.x - posInt.x, source.y - posInt.y, source.z - posInt.z);
								glm::intersectRayTriangle(posInt, toSource,
									world2[1].vertA, world2[1].vertB, world2[1].vertC, result2);
								//if triangle 2 is in between, create shadow
								if (result2.z > 0) {
									if (result2.y <= 1 && result2.y >= 0) {
										if ((1 - result2.y) >= result2.x) {
											if (result2.x > 0) {
												glm::vec3 ambient = ambientStrength * lightColor;

												glm::vec3 normal = glm::normalize(lerpf);
												glm::vec3 lightDir = glm::normalize(source - posInt);

												float diff = std::max(dot(normal, lightDir), 0.0f);
												glm::vec3 diffuse = diff * lightColor;

												glm::vec3 viewDir = glm::normalize(e - posInt);
												glm::vec3 reflectDir = reflect(lightDir, normal);
												float spec = pow(std::max(dot(viewDir, reflectDir), 0.0f), 32);
												glm::vec3 specular = diffuse * specularStrength * spec * lightColor;

												glm::vec3 resultC = (ambient + diffuse + specular)*world2[0].color;

												rgb_t color = make_colour(((factor*result.z) * (255 * resultC.r)), ((factor*result.z) * (255 * resultC.g)),
													((factor*result.z) * (255 * resultC.b)));
												image.set_pixel(x, 960 - y, color);
											}
										}
									}
								}
								// triangle 2 is not in the path
								else {
									glm::vec3 ambient = ambientStrength * lightColor;

									glm::vec3 normal = glm::normalize(lerpf);
									glm::vec3 lightDir = glm::normalize(source - posInt);

									float diff = std::max(dot(normal, lightDir), 0.0f);
									glm::vec3 diffuse = diff * lightColor;

									glm::vec3 viewDir = glm::normalize(e - posInt);
									glm::vec3 reflectDir = reflect(lightDir, normal);
									float spec = pow(std::max(dot(viewDir, reflectDir), 0.0f), 32);
									glm::vec3 specular = diffuse * specularStrength * spec * lightColor;

									glm::vec3 resultC = (ambient + diffuse + specular)*world2[0].color;

									//rgb_t color = make_colour((255 * world2[0].color.r), (255 * world2[0].color.g), (255 * world2[0].color.b));
									rgb_t color = make_colour((255 * resultC.r), (255 * resultC.g), (255 * resultC.b));
									image.set_pixel(x, 960 - y, color);
								}
							}
						}
					}
				}
			}

			//if triangle 2 is intersected
			if (result3.z > 0) {
				if (result3.y <= 1 && result3.y >= 0) {
					if ((1 - result3.y) >= result3.x) {
						if (result3.x > 0) {
							// if triangle 2 is in front of triangle 1
							if (result3.z < result.z) {
								glm::vec3 ambient = ambientStrength * lightColor;

								glm::vec3 normal = glm::normalize(lerpfB);
								glm::vec3 lightDir = glm::normalize(source - posInt2);

								float diff = std::max(dot(normal, lightDir), 0.0f);
								glm::vec3 diffuse = diff * lightColor;

								glm::vec3 viewDir = glm::normalize(e - posInt2);
								glm::vec3 reflectDir = reflect(lightDir, normal);
								float spec = pow(std::max(dot(viewDir, reflectDir), 0.0f), 32);
								glm::vec3 specular = diffuse * specularStrength * spec * lightColor;

								glm::vec3 resultC = (ambient + diffuse + specular)*world2[1].color;

								rgb_t color = make_colour((255 * resultC.r), (255 * resultC.g), (255 * resultC.b));
								//rgb_t color = make_colour((255 * world2[1].color.r), (255 * world2[1].color.g), (255 * world2[1].color.b));
								image.set_pixel(x, 960 - y, color);
							}
						}
					}
				}
			}
		}
	}
}

int main(int argc, char** argv) {

    // create an image 1280 pixels wide by 960 pixels tall
    bitmap_image image(1280, 960);
	for (int x = 0; x < 1280; x++) {
		for (int y = 0; y < 960; y++) {
			rgb_t color = make_colour(190, 190, 40);
			image.set_pixel(x, y, color);
		}
	}

	bitmap_image imageP(1280, 960);
	for (int x = 0; x < 1280; x++) {
		for (int y = 0; y < 960; y++) {
			rgb_t color = make_colour(190, 190, 40);
			imageP.set_pixel(x, y, color);
		}
	}
	

    // build world
    

	std::vector<Triangle> world2 = {
		Triangle(glm::vec3(-2, -1, 2), glm::vec3(7, -1, 10), glm::vec3(0, 5, 10), glm::vec3(-1,-1,0), glm::vec3(1,-1,0), glm::vec3(0,1,0), glm::vec3(0.5,0,1)),
		Triangle(glm::vec3(-0.5, -0.5, 1), glm::vec3(0.5, -0.5, 1), glm::vec3(0, 0.5, 1), glm::vec3(-1,-1,0), glm::vec3(1,-1,0), glm::vec3(0,1,0), glm::vec3(0,1,0))

	};
	
    // render the world
    render(image, world2);
	renderP(imageP, world2);

	
    image.save_image("ray-traced.bmp");
	imageP.save_image("ray-tracedP.bmp");
    std::cout << "Success" << std::endl;
}


